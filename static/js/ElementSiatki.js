﻿function Element() {

    var geometry;
    var lineMaterial;
    var line;

    var element = new THREE.PlaneGeometry(50, 50, 1, 1);
    var material;
    var mesh;
    var container = new THREE.Object3D();
    
    Math.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };

    function generate() {
        
        lineMaterial = new THREE.LineBasicMaterial({ color: 0x000000});
        geometry = new THREE.Geometry();

        // x,y,z

        geometry.vertices.push(new THREE.Vector3(-25, 0, -25));
        geometry.vertices.push(new THREE.Vector3(25, 0, -25));

        geometry.vertices.push(new THREE.Vector3(-25, 0, 25));
        geometry.vertices.push(new THREE.Vector3(25, 0, 25));


        geometry.vertices.push(new THREE.Vector3(-25, 0, -25));
        geometry.vertices.push(new THREE.Vector3(-25, 0, 25));

        geometry.vertices.push(new THREE.Vector3(25, 0, -25));
        geometry.vertices.push(new THREE.Vector3(25, 0, 25));

        line = new THREE.Line(geometry, lineMaterial);




        material = new THREE.MeshBasicMaterial( {color: 0x00ff00, side: THREE.DoubleSide} );
        mesh = new THREE.Mesh(element, material);
        mesh.rotateX(Math.radians(90));

        container.add(line);
        container.add(mesh);

    }

    generate();


    this.getElement = function () {
        return container;
    }

}