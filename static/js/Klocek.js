function Klocek() {

    var singleMesh;

    this.makeKlocek = function (kX, kY, kZ, name, scale) {
      //  var container = new THREE.Object3D();

        var mat = new THREE.MeshBasicMaterial({
            color: 0xff0000, side: THREE.DoubleSide, wireframe: false
        });


        if (name == "newKlocek") {
            var geometryA = new THREE.BoxGeometry(50, 50, scale * 50, 1, 1, 1);
        } else {
            var geometryA = new THREE.BoxGeometry(50, 50, 50, 1, 1, 1);
        }
        
        var geometryB = new THREE.CylinderGeometry(8, 12, 12);


        var meshA = new THREE.Mesh(geometryA);
        var meshB = new THREE.Mesh(geometryB);



        meshA.position.set(kX, 26, kZ);
        meshB.position.set(kX, 56, kZ);


        var singleGeometry = new THREE.Geometry();

        meshA.updateMatrix();
        singleGeometry.merge(meshA.geometry, meshA.matrix);

        meshB.updateMatrix();
        singleGeometry.merge(meshB.geometry, meshB.matrix);

        singleMesh = new THREE.Mesh(singleGeometry, mat);
        singleMesh.name = name;


        //container.add(singleMesh);

        return singleMesh;

    }


}