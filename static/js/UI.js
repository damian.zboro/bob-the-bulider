﻿document.getElementById("bt3").addEventListener("click", function () {

    var login = document.getElementById("login").value;
    var passwd = document.getElementById("pass").value;

    //logowanie
    console.log("L = " + login + "  H = " + passwd);

    client.emit("login", {
        login: login,
        passwd: passwd,
        action: "login",
    })

    client.on("canLog", function (data) {
        console.log(data);
        var loadScene = new Scene();
        var scene = loadScene.getScene();


        if (data.can == true) {

            document.getElementById("log").style.display = "none";
            document.getElementById("bt5").style.display = "block";
            document.getElementById("menu").style.display = "block";

            client.on("setBul", function (data) {
                console.log(data);

                var usrTabs = data.data.data;
                var boxUsr = document.getElementById("usrBox");

                for (var i = 0; i < usrTabs.length; i++) {
                    console.log(usrTabs[i]);

                    var btUsr = document.createElement("button");
                    btUsr.setAttribute("class", "btBox");
                    btUsr.setAttribute("value", usrTabs[i].building);
                    btUsr.innerHTML = usrTabs[i].login;
                    btUsr.addEventListener("click", function () {

                        document.getElementById("bulBox").innerHTML = "";

                        console.log(JSON.parse(this.value));

                        var usrTabs = JSON.parse(this.value);
                        
                            var boxBul = document.getElementById("bulBox");
        
                            for (var j = 0; j < usrTabs.length; j++) {

                                var btBul = document.createElement("button");
                                btBul.setAttribute("class", "btBox");
                                btBul.setAttribute("value", JSON.stringify(usrTabs[j]));
                                btBul.innerHTML = "Budowla " + j;
                                btBul.addEventListener("click", function () {

                                    console.log(JSON.parse(this.value));
                                    var bulTab = JSON.parse(this.value);

                                    for (var i = 0; i < bulTab.length; i++) {
                                        console.log(bulTab[i].x);
                                        
                                        var klocek = new Klocek();
                                        var newKl = klocek.makeKlocek(bulTab[i].x, bulTab[i].y, bulTab[i].z, "klocek");
                                        scene.add(newKl);
                                        
                                    }


                                })
        
                                boxBul.appendChild(btBul);
                            }
                         

                    })


                    boxUsr.appendChild(btUsr);
                }
            })

            main = new Main(scene);

        } else if (data.can == false) {
            alert("Aktualnie ktoś już jest zalogowany na to konto.");
        } else if (data.can == "NoUser") {
            alert("Nie ma takiego usera.");
        }
    })





})


document.getElementById("bt4").addEventListener("click", function () {

    var login = document.getElementById("login").value;
    var passwd = document.getElementById("pass").value;

    //rejestracja
    console.log("L = " + login + "  H = " + passwd);

    if (login == "" || passwd == "") {
        alert("Brak loginu lub hasła.");
    } else {

        client.emit("rejestracja", {
            login: login,
            passwd: passwd,
            action: "rejestracja",
        })

        alert("Pomyślnie utworzono użytkownika.");
    }


})

document.getElementById("bt5").addEventListener("click", function () {
    //save
    var login = document.getElementById("login").value;
    var passwd = document.getElementById("pass").value;

    // IO


    if (myTab.length == 0) {
        alert("Brak budowli do zapisania.");
    } else {

        console.log(JSON.stringify(myTab));

        client.emit("save", {
            login: login,
            passwd: passwd,
            action: "save",
            tab: JSON.stringify(myTab),

        })
    }


})