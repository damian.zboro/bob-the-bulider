/*
    tutaj sterujemy dzia�aniem projektu
    tu powinny si� znale��:
    funkcja inicjuj�ca plansz� z element�w siatki
    publiczne funkcje odpowiedzialne za budowanie i sterowanie elementami projektu
    te funkcje uruchamiamy w Main.js podczas obs�ugi klikni�cia w raycasterze 
*/
function Game() {

    var fieldTab = [];
    var newEl;

    function makeTab() {

        var elX = 50;
        var elZ = 50;

        for (var i = 1; i < 16; i++) {
            for (var j = 1; j < 16; j++) {
                newEl = new Element();
                newEl.getElement().position.set(elX * j, 0, elZ * i);
                fieldTab.push(newEl.getElement());
            }
        }

    }

    makeTab();

    this.retTab = function () {
        return fieldTab;
    }



}