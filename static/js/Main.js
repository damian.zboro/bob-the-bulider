function Main(scene) {

    var z = window.innerWidth;
    var zz = window.innerHeight;
    var renderer = new THREE.WebGLRenderer();
    var axis = new THREE.AxisHelper(200);


    var intro;
    var newField;
    var klocek;
    var change;
    var colorNr = 0;
    var scale = 2;


    var fieldPos;
    var delKlocek;
    var kloc;


    var myKloc = [];

    intro = new Intro(scene);


    var camera = new THREE.OrthographicCamera(
    window.innerWidth / -1,
    window.innerWidth / 1,
    window.innerHeight / 1,
    window.innerHeight / -1,
    0,
    10000);

    Math.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };


    K = 90;
    gora = false;
    dol = false;
    prawo = false;
    lewo = false;

    ///////////////////////////////
    ///////////////////////////////
    ///////////////////////////////


    function plansza() {

        newField = new Game();

        for (var i = 0; i < newField.retTab().length; i++) {
            newField.retTab()[i].name = "field";
            scene.add(newField.retTab()[i]);

        }

    }


    function init() {

        renderer.setClearColor(0xffffff);
        renderer.setSize(z, zz);
        scene.add(axis);
        document.getElementById("l").appendChild(renderer.domElement);

        camera.position.x = 3;
        camera.position.y = 30;
        camera.position.z = 200;

        camera.lookAt(scene.position);

        intro.setIntro();
        function animateScene() {

            requestAnimationFrame(animateScene);
            renderer.render(scene, camera);


            if (gora == true) {
                camera.position.y = camera.position.y + 5;
                camera.lookAt(scene.position);
            }

            else if (dol == true) {
                camera.position.y = camera.position.y - 5;
                camera.lookAt(scene.position);
            }

            else if (prawo == true) {
                K -= 2;
                camera.position.x = 200 * Math.cos(Math.radians(K));
                camera.position.z = 200 * Math.sin(Math.radians(K));
                camera.lookAt(scene.position);
            }
            else if (lewo == true) {
                K += 2;
                camera.position.x = 200 * Math.cos(Math.radians(K));
                camera.position.z = 200 * Math.sin(Math.radians(K));
                camera.lookAt(scene.position);
            }




        }
        animateScene();



    }

    init();


    function cameraUp() {
        camera.position.x = 93;
        camera.position.y = 205;
        camera.position.z = -176;
        camera.updateProjectionMatrix();
    }




    document.addEventListener("mousedown", onMouseDown, false);
    var raycaster = new THREE.Raycaster();
    var mouseVector = new THREE.Vector2();


    function onMouseDown(event) {

        mouseVector.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouseVector.y = -(event.clientY / window.innerHeight) * 2 + 1;

        raycaster.setFromCamera(mouseVector, camera);
        var intersects = raycaster.intersectObjects(scene.children, true);

        
        //console.log(intersects.length)
        
        if (intersects.length > 0) {
            //console.log(intersects[0].object);
            //console.log(intersects[0].object.parent.name);
        }

        if (intersects.length > 0 && intersects[0].object.name == "napis") {
            scene.children = [];
            plansza();
            cameraUp();
        }


        if (intersects.length > 0 && intersects[0].object.parent.name == "field") {

            //console.log(intersects[0].object.parent.position);
            fieldPos = intersects[0].object.parent.position;
            klocek = new Klocek();
            var newKl = klocek.makeKlocek(fieldPos.x, 1, fieldPos.z, "klocek");
            scene.add(newKl);
            //console.log(newKl.uuid)

            var thisKloc = {
                x: fieldPos.x,
                y: 1,
                z: fieldPos.z,
            }

            myTab.push(thisKloc);
            //console.log(myTab);


            // IO
            client.emit("bulid", {
                posX: fieldPos.x,
                posY: 1,
                posZ: fieldPos.z,
                uuid: newKl.uuid
            })

            client.on("bulid", function (data) {
                console.log(data.posX + " - " + data.posY + " - " + data.posZ + " - " + data.uuid)

                klocek = new Klocek();
                var newKl = klocek.makeKlocek(data.posX, data.posY, data.posZ, "klocek")
                newKl.uuid = data.uuid;
                scene.add(newKl);
            })

        }

        if (intersects.length > 0 && intersects[0].object.name == "klocek") {
            colorNr = 0;
            change = intersects[0].object;
            console.log(change);
        }



    }



    document.addEventListener("keydown", onKeyDown, false);
    document.addEventListener("keyup", onKeyUp, false);



    var colors = [
        { color: "yellow", hex: "0xFFFF00" },
        { color: "black", hex: "0x000000" },
        { color: "blue", hex: "0x0000FF" },
        { color: "pink", hex: "0xFF00FF" }
    ];


    function onKeyDown(event) {

        var keyCode = event.which;

        //console.log(keyCode); 

        switch (keyCode) {

            case 38:
                gora = true
                break;

            case 40:
                dol = true
                break;

            case 37:
                lewo = true
                break;

            case 39:
                prawo = true
                break;

            case 27:
                //ESC
                break;







            case 81:               
                change.material.color.setHex(colors[colorNr].hex);

                client.emit("color", {
                    uuid: change.uuid,
                    col: colors[colorNr].hex,
                })

                client.on("color", function (data) {
                    console.log(data.col + " - " + data.uuid)
                    //console.log(scene.children);
                    for (var i = scene.children.length -1; i >= 0; i--) {
                        if (scene.children[i].type =="Mesh" && scene.children[i].uuid == data.uuid) {
                            scene.children[i].material.color.setHex(data.col);
                            break;
                        }

                    }

                })


                colorNr = colorNr + 1;
                if (colorNr == colors.length) {
                    colorNr = 0;
                }
                
                break;
            case 87:
                break;
            case 69:
                //
                break;

        }

    }

    function onKeyUp(event) {

        var keyCode = event.which;

        switch (keyCode) {

            case 38:
                gora = false;
                //console.log(camera.position)
                break;

            case 40:
                dol = false;
                //console.log(camera.position)
                break;

            case 37:
                lewo = false;
                break;

            case 39:
                prawo = false;
                break;

        }
    }







    ///////////////////////////////
    ///////////////////////////////
    ///////////////////////////////


}