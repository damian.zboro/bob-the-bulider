function Intro(scene) {
    this.setIntro = function () {

        var mesh;
        var mat;
        mat = new THREE.MeshBasicMaterial({
            color: 0x0000ff, side: THREE.DoubleSide, wireframe: true
        });
        var napisTab = ["B", "O", "B", " ", "T", "H", "E", " ", "B", "I", "L", "D", "E", "R"];

        var setX = -500;

        //mesh normal material

        function generuj(obj, litera) {

            var textGeometry = new THREE.TextGeometry(
            litera,
            {
                font: obj,
                height: 70
            });


            mesh = new THREE.Mesh(textGeometry, mat);
            mesh.position.x = setX;
            mesh.position.z = 0;
            mesh.name = "napis";
            scene.add(mesh);
        }


        var loader = new THREE.FontLoader();
        loader.load("font.json",
            function (obj) {

                //console.log(obj)

                for (var i = 0; i < napisTab.length; i++) {
                    generuj(obj, napisTab[i], setX);
                    setX = setX + 70;
                }

            },
            function (xhr) {
                console.log((xhr.loaded / xhr.total * 100) + '% loaded');
            },
            function (xhr) {
                console.log(xhr.error)
            })




    }
}